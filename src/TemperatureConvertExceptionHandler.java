
public class TemperatureConvertExceptionHandler extends Exception 
{
	String message;
	
	public TemperatureConvertExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}

}