
public class Convert 
{
	public double ConvertCtoF(double C) throws TemperatureConvertExceptionHandler
	{
		if(C > 100)
		{
			throw new TemperatureConvertExceptionHandler("Celsius number is above Boiling Point");
		}
		if(C < -273.15)
		{
			throw new TemperatureConvertExceptionHandler("Celsius number is over Absolute Zero");
		}
		return (((C*9)/5)+32);
	}
	public double ConvertFtoC(double F) throws TemperatureConvertExceptionHandler
	{
		if(F>212)
		{
			throw new TemperatureConvertExceptionHandler("Fahrenheit number is bigger than Boiling Point");
		}
		if(F < -459.67)
		{
			throw new TemperatureConvertExceptionHandler("Fahrenheit number is bigger than absolute zero");
		}
		return (((F - 32)* 5)/9);
	}
}
