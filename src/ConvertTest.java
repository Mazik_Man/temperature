import junit.framework.TestCase;

public class ConvertTest extends TestCase
{
	//Equivilance Partition for ConvertCtoF
	
	//Test #: 1
	// Objective: Test Invalid Convert from Celsius to Fahrenheit
	// Input(s): Celsius -274.20
	// Expected Output: Exception Message "Celsius number is over Absolute Zero"
	public void testconvertTemperature0001()
	{
		
		Convert testObject = new Convert();
		try 
		{
			double celsius = testObject.ConvertCtoF(-274.20);
			fail("Should not get here ... Exception is expected");
		}
		
		catch (TemperatureConvertExceptionHandler e)
		{
			assertEquals("Celsius number is over Absolute Zero", e.getMessage());
		}

	}
	
	//Test #: 2
	// Objective: Valid Convert from Celsius to Fahrenheit
	// Input(s): Celsius -272.02
	// Expected Output: Fahrenheit -457.636
	public void testconvertTemperature0002()
	{
		
		Convert testObject = new Convert();
		try 
		{
			assertEquals(-457.636, testObject.ConvertCtoF(-272.02), 0.01);
		}
		
		catch (TemperatureConvertExceptionHandler e)
		{
			fail("Should not get here ... Exception is expected");
		}

	}
	
	//Test #: 3
	// Objective: Valid Convert from Celsius to Fahrenheit
	// Input(s): Celsius -16.55
	// Expected Output: Fahrenheit 2.21
		public void testconvertTemperature0003()
		{
			
			Convert testObject = new Convert();
			try 
			{
				
				assertEquals(2.21, testObject.ConvertCtoF(-16.55), 0.01);
			}
			
			catch (TemperatureConvertExceptionHandler e)
			{
				fail("Should not get here ... Exception is expected");
			}

		}
		//Test #: 4
		// Objective: Valid Convert from Celsius to Fahrenheit
		// Input(s): Celsius 1.29
		// Expected Output: Fahrenheit 34.32
			public void testconvertTemperature0004()
			{
				
				Convert testObject = new Convert();
				try 
				{
					
					assertEquals(34.32, testObject.ConvertCtoF(1.29), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
	//Test #: 5
		// Objective: Test Invalid Convert from Celsius to Fahrenheit
		// Input(s): Celsius 101.78
		// Expected Output: Exception Message "Celsius number is over Boiling Point"
		public void testconvertTemperature0005()
		{
			
			Convert testObject = new Convert();
			try 
			{
				double celsius = testObject.ConvertCtoF(101.78);
				fail("Should not get here ... Exception is expected");
			}
			
			catch (TemperatureConvertExceptionHandler e)
			{
				assertEquals("Celsius number is above Boiling Point", e.getMessage());
			}

		}
		
		//Equivilance Partition for ConvertFtoC	
		
		//Test #: 1
		// Objective: Test Invalid Convert from Fahrenheit to Celsius
		// Input(s): Fahrenheit -469.67
		// Expected Output: Exception Message "Fahrenheit number is over Absolute Zero"
		public void testconvertTemperature0006()
		{
			
			Convert testObject = new Convert();
			try 
			{
				double Fahreheit = testObject.ConvertFtoC(-469.67);
				fail("Should not get here ... Exception is expected");
			}
			
			catch (TemperatureConvertExceptionHandler e)
			{
				assertEquals("Fahrenheit number is bigger than absolute zero", e.getMessage());
			}

		}
		
		//Test #: 2
		// Objective: Valid Convert from Fahrenheit to Celsius
		// Input(s): Fahrenheit -362.27
		// Expected Output: Celsius -219.03
		public void testconvertTemperature0007()
		{
			
			Convert testObject = new Convert();
			try 
			{
				assertEquals(-219.03, testObject.ConvertFtoC(-362.27), 0.01);
			}
			
			catch (TemperatureConvertExceptionHandler e)
			{
				fail("Should not get here ... Exception is expected");
			}

		}
		
		//Test #: 3
		// Objective: Valid Convert from  Fahrenheit to Celsius
		// Input(s): Fahrenheit 3.21
		// Expected Output: Celsius -15.99
			public void testconvertTemperature0008()
			{
				
				Convert testObject = new Convert();
				try 
				{
					
					assertEquals(-15.99, testObject.ConvertFtoC(3.21), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 4
			// Objective: Valid Convert from Fahrenheit to Celsius
			// Input(s): Fahrenheit 35.2
			// Expected Output: Celsius 1.77
				public void testconvertTemperature0009()
				{
					
					Convert testObject = new Convert();
					try 
					{
						
						assertEquals(1.77, testObject.ConvertFtoC(35.2), 0.01);
					}
					
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
		//Test #: 5
			// Objective: Test Invalid Convert from Fahrenheit to Celsius
			// Input(s): Fahrenheit 215.38
			// Expected Output: Exception Message "Fahrenheit number is over Boiling Point"
			public void testconvertTemperature0010()
			{
				
				Convert testObject = new Convert();
				try 
				{
					double fahrenheit = testObject.ConvertFtoC(215.38);
					fail("Should not get here ... Exception is expected");
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					assertEquals("Fahrenheit number is bigger than Boiling Point", e.getMessage());
				}

			}
		
			//Boundary Value Analysis for ConvertCtoF
			
			//Test #: 1
			// Objective: Test Invalid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -273.16
			// Expected Output: Exception Message "Celsius number is over Absolute Zero"
			public void testconvertTemperature0011()
			{
				
				Convert testObject = new Convert();
				try 
				{
					double celsius = testObject.ConvertCtoF(-273.16);
					fail("Should not get here ... Exception is expected");
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					assertEquals("Celsius number is over Absolute Zero", e.getMessage());
				}

			}
			//Test #: 2
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -273.15
			// Expected Output: Fahrenheit -459.67
			public void testconvertTemperature0012()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(-459.67, testObject.ConvertCtoF(-273.15), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 3
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -273.14
			// Expected Output: Fahrenheit -459.652
			public void testconvertTemperature0013()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(-459.652, testObject.ConvertCtoF(-273.14), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 4
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -17.76
			// Expected Output: Fahrenheit 0.032
			public void testconvertTemperature0014()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(0.032, testObject.ConvertCtoF(-17.76), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 5
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -17.77
			// Expected Output: Fahrenheit 0.014
			public void testconvertTemperature0015()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(0.014, testObject.ConvertCtoF(-17.77), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 6
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -17.78
			// Expected Output: Fahrenheit -0.004
			public void testconvertTemperature0016()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(-0.004, testObject.ConvertCtoF(-17.78), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 7
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius -0.01
			// Expected Output: Fahrenheit 31.982
			public void testconvertTemperature0017()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(31.982, testObject.ConvertCtoF(-0.01), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 8
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius 0.00
			// Expected Output: Fahrenheit 32.00
			public void testconvertTemperature0018()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(32.00, testObject.ConvertCtoF(0.00), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 9
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius 0.01
			// Expected Output: Fahrenheit 32.018
			public void testconvertTemperature0019()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(32.018, testObject.ConvertCtoF(0.01), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 10
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius 99.99
			// Expected Output: Fahrenheit 211.982
			public void testconvertTemperature0020()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(211.982, testObject.ConvertCtoF(99.99), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 11
			// Objective: Valid Convert from Celsius to Fahrenheit
			// Input(s): Celsius 100.00
			// Expected Output: Fahrenheit 212.00
			public void testconvertTemperature0021()
			{
				
				Convert testObject = new Convert();
				try 
				{
					assertEquals(212.00, testObject.ConvertCtoF(100.00), 0.01);
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					fail("Should not get here ... Exception is expected");
				}

			}
			//Test #: 12
			// Objective: Test Invalid Convert from Celsius to Fahrenheit
			// Input(s): Celsius 100.01
			// Expected Output: Exception Message "Celsius number is above Boiling Point"
			public void testconvertTemperature0022()
			{
				
				Convert testObject = new Convert();
				try 
				{
					double celsius = testObject.ConvertCtoF(100.01);
					fail("Should not get here ... Exception is expected");
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					assertEquals("Celsius number is above Boiling Point", e.getMessage());
				}

			}
		
			//Boundary Value Analysis for ConvertFtoC
			
			//Test #: 1
			// Objective: Test Invalid Convert from Fahrenheit to Celsius
			// Input(s): Fahrenheit -459.68
			// Expected Output: Exception Message "Fahrenheit number is bigger than absolute zero"
			public void testconvertTemperature0023()
			{
				
				Convert testObject = new Convert();
				try 
				{
					double fahrenheit = testObject.ConvertFtoC(-459.68);
					fail("Should not get here ... Exception is expected");
				}
				
				catch (TemperatureConvertExceptionHandler e)
				{
					assertEquals("Fahrenheit number is bigger than absolute zero", e.getMessage());
				}

			}
			//Test #: 2
			// Objective: Valid Convert from Fahrenheit to Celsius
			// Input(s): Fahrenheit -459.67
			// Expected Output: Celsius −273.15
				public void testconvertTemperature00024()
				{
					
					Convert testObject = new Convert();
					try 
					{
						
						assertEquals(-273.15, testObject.ConvertFtoC(-459.67), 0.01);
					}
					
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 3
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit -459.66
				// Expected Output: Celsius -273.144444
					public void testconvertTemperature00025()
					{
						
						Convert testObject = new Convert();
						try 
						{
							
							assertEquals(-273.144444, testObject.ConvertFtoC(-459.66), 0.01);
						}
						
						catch (TemperatureConvertExceptionHandler e)
						{
							fail("Should not get here ... Exception is expected");
						}

					}
				//Test #: 4
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit -0.01
				// Expected Output: Celsius -17.78
				public void testconvertTemperature00026()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(-17.78, testObject.ConvertFtoC(-0.01), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 5
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 0.00
				// Expected Output: Celsius -17.77
				public void testconvertTemperature00027()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(-17.77, testObject.ConvertFtoC(0.00), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 6
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 0.01
				// Expected Output: Celsius -17.77222
				public void testconvertTemperature00028()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(-17.77222, testObject.ConvertFtoC(0.00), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 7
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 31.99
				// Expected Output: Celsius -0.0055555556
				public void testconvertTemperature00029()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(-0.0055555556, testObject.ConvertFtoC(31.99), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 8
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 32.00
				// Expected Output: Celsius 0
				public void testconvertTemperature00030()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(0.00, testObject.ConvertFtoC(32.00), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 9
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 32.01
				// Expected Output: Celsius 0.0055555556
				public void testconvertTemperature00031()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(0.0055555556, testObject.ConvertFtoC(32.01), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 10
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 211.99
				// Expected Output: Celsius 99.9944444
				public void testconvertTemperature00032()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(99.9944444, testObject.ConvertFtoC(211.99), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Test #: 11
				// Objective: Valid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 212.00
				// Expected Output: Celsius 100.00
				public void testconvertTemperature00033()
				{
							
					Convert testObject = new Convert();
					try 
					{
								
						assertEquals(100.00, testObject.ConvertFtoC(212.00), 0.01);
					}
							
					catch (TemperatureConvertExceptionHandler e)
					{
						fail("Should not get here ... Exception is expected");
					}

				}
				//Boundary Value Analysis for ConvertFtoC
				
				//Test #: 12
				// Objective: Test Invalid Convert from Fahrenheit to Celsius
				// Input(s): Fahrenheit 212.01
				// Expected Output: Exception Message "Fahrenheit number is bigger than Boiling Point"
				public void testconvertTemperature0034()
				{
					
					Convert testObject = new Convert();
					try 
					{
						double fahrenheit = testObject.ConvertFtoC(212.01);
						fail("Should not get here ... Exception is expected");
					}
					
					catch (TemperatureConvertExceptionHandler e)
					{
						assertEquals("Fahrenheit number is bigger than Boiling Point", e.getMessage());
					}

				}
}
